const mysql = require('mysql');
const _ = require('underscore');
const uuid4 = require('uuid').v4;
const config = require('/app/ab-app-config.json');
const pool = mysql.createPool(config.mysql);

class API {
	constructor() {
	}
	loadMapData() {
		return new Promise((resolve, reject) => {
			let mapData = [];
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('SELECT json_data FROM MAP UNION ALL SELECT json_data FROM MAP_ATM', (err, result) => {
					if (err) reject(err);
					for (const e in result) {
						mapData.push(JSON.parse(result[e].json_data));
					}
					resolve(mapData);
				});
				db.release();
			});
		});
	}
	cacheAdd(address, geo) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('INSERT INTO MAP_GEO_CACHE(address,cache) VALUES (?,?)', [address.trim().toLowerCase(), geo], err => {
					if (err && err.code != 'ER_DUP_ENTRY') reject(err);
					resolve({
						status: 'ok'
					});
				});
				db.release();
			});
		});
	}
	cacheGet(address) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('SELECT cache FROM MAP_GEO_CACHE WHERE ADDRESS = ?', address, (err, result) => {
					if (err) reject(err);
					let data = { status: 'NOT_FOUND' };

					for (const e in result) {
						data = JSON.parse(result[e].cache);
					}
					resolve(data);
				});
				db.release();
			});
		});
	}
	logSearch(search, geo, isCached) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('INSERT INTO MAP_SEARCH_LOG(search,result,cache) VALUES (?,?,?)', [search, geo, isCached], err => {
					if (err) reject(err);
					resolve();
				});
				db.release();
			});
		});
	}
	search(query) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);

				let sql = 'SELECT t.json_data FROM (SELECT json_data FROM MAP UNION ALL SELECT json_data FROM MAP_ATM) t where ';
				let params = [];
				let q = `%${query}%`;

				switch (query) {
				case '[empty]':
					sql += 't.json_data->>\'$.latitude\' is null or t.json_data->>\'$.longitude\'is null ';
					break;
				default:
					if (query.startsWith('=')) {
						q = query.substring(1);
						sql += 't.json_data->>\'$.description\' = ? or ';
						params.push(q);
					} else {
						sql += ' upper(t.json_data->>\'$.description\') like upper(?) or ';
						params.push(q);
					}
					sql += ' upper(t.json_data->>\'$.uid\') = upper(?)';
					params.push(query);
				}
				sql += ' LIMIT 10';

				db.query(sql, params, (err, result) => {
					if (err) reject(err);
					let data = [];
					for (const e in result) {
						data.push(result[e].json_data);
					}
					resolve(data);
				});
				db.release();
			});
		});
	}
	find(id) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('SELECT json_data FROM MAP WHERE JSON_EXTRACT(json_data, \'$._id\') = ? UNION ALL SELECT json_data FROM MAP_ATM WHERE JSON_EXTRACT(json_data, \'$._id\') = ?', [id, id], (err, result) => {
					if (err) reject(err);
					if (result.length == 0) reject('not found');
					for (const e in result) {
						resolve(result[e].json_data);
					}
				});
				db.release();
			});
		});
	}
	check(params) {
		if (!params.description) throw 'no description';
		if (!params.city) throw 'no city';
		if (!params.address) throw 'no address';
		if (!params.region) throw 'no region';
		if (!params.division) throw 'no division';
		if (!params.division.match('[0-3]')) throw 'division not match [0-3]';
		if (params.division == '3') {
			if (!params.uid) throw 'no uid';
		}
		if (!params.priority) params.priority = '999';
		if (!params.priority.match('[0-9]')) throw 'priority not match [0-9]';
	}
	save(params) {
		return new Promise((resolve, reject) => {

			this.check(params);

			if (params._id) {
				return this.find(params._id)
					.then(d => {
						const data = JSON.parse(d);

						params._created = data._created;
						params._updated = new Date().getTime();

						pool.getConnection((err, db) => {
							if (err) reject(err);
							let sql = 'UPDATE MAP SET json_data = ? WHERE JSON_EXTRACT(json_data, \'$._id\') = ?';
							if (params.division == 3) {
								sql = 'UPDATE MAP_ATM SET json_data = ? WHERE JSON_EXTRACT(json_data, \'$._id\') = ?';
							}
							db.query(sql, [JSON.stringify(params), params._id], (err, result) => {
								if (err) reject(err);
								resolve(result);
							});
							db.release();
						});
					})
					.catch(err => {
						reject(err);
					});
			} else {
				let item = _.clone(params);

				item._id = uuid4();
				item._created = new Date().getTime();
				item._updated = item._created;

				pool.getConnection((err, db) => {
					if (err) reject(err);
					let sql = 'INSERT INTO MAP VALUES (?)';
					if (params.division == 3) {
						sql = 'INSERT INTO MAP_ATM VALUES (?)';
					}
					db.query(sql, JSON.stringify(item), (err, result) => {
						if (err) reject(err);
						resolve(result);
					});
					db.release();
				});
			}
		});
	}
	delete(id) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				if (err) reject(err);
				db.query('DELETE FROM MAP WHERE JSON_EXTRACT(json_data, \'$._id\') = ?', id, (err, result) => {
					if (err) reject(err);
					resolve(result);
				});
				db.query('DELETE FROM MAP_ATM WHERE JSON_EXTRACT(json_data, \'$._id\') = ?', id, (err, result) => {
					if (err) reject(err);
					resolve(result);
				});
				db.release();
			});
		});
	}
	distance(lat1, lng1, lat2, lng2) {
		const rad = x => {
			return (x * 3.1415926535897931) / 180;
		};
		const R = 6371;
		const dLat = rad(lat1 - lat2);
		const dLong = rad(lng1 - lng2);
		const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
		return (R * c);
	}
	list(mapData, params) {
		return new Promise(resolve => {
			let matchList = [];
			let response;
			let isMatch;
			let atmType;

			for (const e in mapData) {
				const i = mapData[e];

				isMatch = false;

				if (i.division == 0 && params.branch == 1) isMatch = true;
				else if (i.division == 1 && params.ikc == 1) isMatch = true;
				else if (i.division == 2 && params.pos == 1) isMatch = true;
				else if (i.division == 3) {
					switch (i.description) {
					case 'Альфабанк': atmType = 1; break;
					case 'Укрсоцбанк': atmType = 2; break;
					default: atmType = 3;
					}
					if (params.atm_abu == 1 && atmType == 1) isMatch = true;
					if (params.atm_usb == 1 && atmType == 2) isMatch = true;
					if (params.atm_oth == 1 && atmType == 3) isMatch = true;

					if (isMatch && params.atm_filter_24x7 == 1) {
						isMatch = (i.scheduleType == '24h');
					}

					if (isMatch && params.atm_filter_cashin == 1) {
						isMatch = (i.type == 'Только пополнение' || i.type == 'Пополнение+Снятие');
					}

					if (isMatch && params.atm_filter_cashout == 1) {
						isMatch = (i.type == 'Только снятие' || i.type == 'Пополнение+Снятие');
					}
				}

				if (isMatch && i.latitude && i.longitude) {
					matchList.push({
						id: i._id + '',
						dist: this.distance(Number(params.lat), Number(params.lng), i.latitude, i.longitude),
						lat: i.latitude,
						lng: i.longitude,
						address: i.address,
						city: i.city,
						razmeshenie: i.razmeshenie,
						schedule: i.schedule,
						division: i.division,
						description: i.description,
						priority: i.priority,
						uid: i.uid,
						atmType: i.type
					});
				}
			}
			response = _.chain(matchList)
				.sortBy('dist')
				.first(params.countLine)
				.value();

			response = _.reduce(response, (result, item, idx) => {
				item.index = ++idx;
				result.push(item);
				return result;
			}, []);

			resolve(response);
		});
	}
}
module.exports = new API();