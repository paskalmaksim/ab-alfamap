const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const csv = require('csv');
const iconv = require('iconv-lite');
const api = require('./api');
const app = express();
const pino = require('pino')({ name: 'alfamap' });
const path = require('path');

const server = {
	filename: __filename,
	host: '127.0.0.1',
	port: 8090,
	context: '/map'
};

/* localstore */
let mapData = [];

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', express.static(path.resolve(__dirname, 'frontend/dist')));

app.get(`${server.context}/admin/api/loadMapData`, (req, res) => {
	res.send('success');
	process.send({ loadMapData: 'cluster update' });
});
app.get(`${server.context}/admin/api/export`, async (req, res) => {
	try {
		const params = req.query;
		const data = await api.loadMapData();

		const type = params.type ? params.type : 'json';

		if (type == 'csv') {
			csv.stringify(data, {
				delimiter: ';', header: 'true', columns: [
					'_id',
					'_created',
					'_updated',
					'region',
					'city',
					'address',
					'description',
					'division',
					'latitude',
					'longitude',
					'priority',
					'razmeshenie',
					'schedule',
					'type',
					'uid'
				]
			}, (err, output) => {
				res.attachment('map.export.csv');
				res.send(iconv.encode(output, 'win1251'));
			});
		} else if (type == 'json') {
			res.attachment('map.export.json');
			res.send(data);
		} else {
			res.send('unknown type');
		}
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err);
	}
});
app.get(`${server.context}/admin/api/all`, async (req, res) => {
	try {
		res.send(JSON.stringify(await api.loadMapData()));
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err);
	}
});
app.get(`${server.context}/admin/api/find`, async (req, res) => {
	try {
		if (!req.query.id) return res.sendStatus(400);

		const data = await api.find(req.query.id);

		if (data) {
			res.send(JSON.parse(data));
		}
		else {
			res.sendStatus(404);
		}
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err);
	}
});
app.post(`${server.context}/admin/api/save`, async (req, res) => {
	try {
		if (!req.body) return res.status(400).send();

		res.send(await api.save(req.body));
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err);
	}
});
app.get(`${server.context}/admin/api/delete/:id`, async (req, res) => {
	try {
		const { id } = req.params;

		if (!id) return res.status(400).send();

		res.send(await api.delete(id));
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err);
	}
});
app.post(`${server.context}/api/logSearch`, async (req, res) => {
	try {
		await api.logSearch(decodeURI(req.body.address), req.body.results, req.body.isCached);
		res.send('OK');
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err.stack);
	}
});
app.get(`${server.context}/api/cache`, async (req, res) => {
	try {
		if (!req.query.address) {
			res.status(400).send('address not set');
			return;
		}
		res.send(await api.cacheGet(req.query.address));
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err.stack);
	}
});
app.post(`${server.context}/api/cache/add`, async (req, res) => {
	try {
		res.send(await api.cacheAdd(decodeURI(req.body.address), req.body.results));
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err.stack);
	}
});
app.get(`${server.context}/api/list`, async (req, res) => {
	try {
		res.send({
			status: 'ok',
			msg: await api.list(mapData, req.query)
		});
	}
	catch (err) {
		pino.error(err);
		res.status(500).send(err.stack);
	}
});

app.get(`${server.context}/api/search`, async (req, res) => {
	res.type('json');

	try {
		const { q } = req.query;

		const results = await api.search(q);

		res.send(JSON.stringify({
			status: 'ok',
			q: q,
			results: results
		}));
	} catch (err) {
		console.error(err);
		res.send(JSON.stringify({
			status: 'error',
			error: err.toString()
		}));
	}
});

const cluster = require('cluster');
if (cluster.isMaster) {
	pino.info(server);

	const cpu_length = require('os').cpus().length;

	for (let i = 0; i < cpu_length; i++) {
		const worker = cluster.fork();

		worker.on('message', msg => {
			pino.info('received message', msg);

			if (msg.test) {
				for (const id in cluster.workers) {
					cluster.workers[id].send(msg);
				}
			} else if (msg.loadMapData) {
				api.loadMapData()
					.then((data) => {
						for (const id in cluster.workers) {
							cluster.workers[id].send({ data: data });
						}
					})
					.catch((err) => {
						pino.error('loadMapData', err);
					});
			}
		});
	}
	/* init */
	api.loadMapData()
		.then((data) => {
			for (const id in cluster.workers) {
				cluster.workers[id].send({ data: data });
			}
		})
		.catch((err) => {
			pino.error(err);
		});
} else {
	process.on('message', msg => {
		if (msg.test) {
			pino.info(`Worker ${process.pid} mapData.length=${mapData.length}`);
		}
		else if (msg.data) {
			mapData = msg.data;
		}
	});
	http.createServer(app).listen(server.port, server.host);
}