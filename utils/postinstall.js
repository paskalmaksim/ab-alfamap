const fs = require('fs');
const path = require('path');
const async = require('async');
const rl = require('readline').createInterface(process.stdin, process.stdout);

const configFile = '/app/ab-app-config.json';
const defaultConfig = {
	'mysql': {
		'host': 'kkipccapp01.alfa.bank.int',
		'user': 'test',
		'password': 'test',
		'database': 'alfamap',
		'connectionLimit': 3
	}
};

let questions = [];

if (!fs.existsSync(path.dirname(configFile))) {
	fs.mkdirSync(path.dirname(configFile));
}
if (fs.existsSync(configFile)) {
	process.exit(0);
}
Object.keys(defaultConfig.mysql).forEach(key => {
	questions.push((callback) => {
		rl.question(`${key}? [${defaultConfig.mysql[key]}]`, args => {
			if (args) defaultConfig.mysql[key] = args;
			callback();
		});
	});
});
questions.push((callback) => {
	console.log(JSON.stringify(defaultConfig));
	callback();
});

const mysql = require('mysql');
let db;

questions.push((callback) => {
	db = mysql.createConnection(defaultConfig.mysql);
	db.connect();
	db.query('SELECT 1');
	callback();
});
questions.push((callback) => {
	db.query(fs.readFileSync(`${__dirname}/postinstall/tables/MAP.sql`, 'utf8'));
	db.query(fs.readFileSync(`${__dirname}/postinstall/tables/MAP_ATM.sql`, 'utf8'));
	db.query(fs.readFileSync(`${__dirname}/postinstall/tables/MAP_GEO_CACHE.sql`, 'utf8'));
	db.query(fs.readFileSync(`${__dirname}/postinstall/tables/MAP_SEARCH_LOG.sql`, 'utf8'));
	callback();
});
questions.push((callback) => {
	rl.question('save config file? [Y/n]', args => {
		if (!args || args == 'Y') {
			fs.writeFileSync(configFile, JSON.stringify(defaultConfig, null, 4));
		}
		callback();
	});
});
questions.push((callback) => {
	console.log('done');
	callback();
	rl.close();
	db.end();
});

async.series(questions);