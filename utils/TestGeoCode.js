const logger = require('pino')({ name: 'TestGeoCode' });

const request = require('request').defaults({
	strictSSL: false,
	proxy: 'http://127.0.0.1:3139',
	pool: {
		maxSockets: 2
	}
});

const options = {
	url: 'https://maps.googleapis.com/maps/api/geocode/json',
	qs: {
		key: 'AIzaSyD8ComL6Ubz-l_6KIbjVjcbSzHL9NyQbHI',
		address: 'Украина, пр. Миру, 32, Покровськ',
		'components': 'components=country:UA',
		'language': 'ru',
		'region': 'ua'
	}
};
request(options, (error, response, body) => {
	try {
		if (error) throw error;
		const data = JSON.parse(body);
		if(data.status != 'OK') throw data;
		logger.info(data.results);
		logger.info('OK');
	} catch (err) {
		logger.error(err);
	}
});
