const XLSX = require('xlsx');

if (process.argv.length != 3) {
	console.error(`no args, example\n ${process.argv[0]} ${process.argv[1]} /some/path/to/xlsfiles`);
	process.exit(-1);
}

const dir = process.argv[2];
const fs = require('fs');
const path = require('path');

const config = require('/app/ab-app-config.json');
const mysql = require('mysql');

const importConfig = {
	'UniversalBank': {
		header: [
			'fake',
			'bank',
			'region',
			'city',
			'address',
			'type',
			'time',
			'id'
		]
	},
	'ATM Ukrsotsbank': {
		header: [
			'id',
			'bank',
			'region',
			'city',
			'address',
			'landmark',
			'time',
			'type'
		]
	},
	'Альфа-Банк': {
		header: [
			'id',
			'bank',
			'region',
			'city',
			'address',
			'time'
		]
	},
	'ВТБ': {
		header: [
			'id',
			'region',
			'city',
			'address',
			'time',
			'type'
		],
		defaults: {
			bank: 'ВТБ'
		}
	},
	'Правекс': {
		header: [
			'bank',
			'region',
			'city',
			'address',
			'time',
			'type',
			'fake',
			'id',
			'id2'
		]
	},
	'Сбер': {
		header: [
			'bank',
			'region',
			'city',
			'address',
			'type',
			'time',
			'id'
		]
	},
	'Терминальное оборудование': {
		header: [
			'bank',
			'region',
			'city',
			'fake',
			'address',
			'landmark',
			'time',
			'id'
		]
	},
	'Укргаз': {
		header: [
			'region',
			'city',
			'bank',
			'fake',
			'fake1',
			'id',
			'address',
			'landmark',
			'time',
			'id2'
		]
	},
	'Укрэксим': {
		header: [
			'bank',
			'region',
			'city',
			'address',
			'type',
			'time',
			'id'
		]
	}
};
/* get current map */
const getMapData = new Promise((resolve, reject) => {
	let mapData = [];
	const db = mysql.createConnection(config.mysql);
	db.query('SELECT json_data FROM MAP_ATM', function (err, result) {
		if (err) reject(err);
		for (const e in result) {
			const a = JSON.parse(result[e].json_data);
			mapData[a.uid] = a;
		}
		resolve(mapData);
	});
	db.end();
});

getMapData
	.then(mapData => {
		console.log(`mapData.length=${Object.keys(mapData).length}`);
		let data = [];
		let dataNotFound = 0;

		fs.readdirSync(dir).forEach(file => {
			try {
				const fullPath = path.join(dir, file);

				let fileConfig = {};

				Object.keys(importConfig).forEach(item => {
					if (file.startsWith(item)) {
						fileConfig = importConfig[item];
						return;
					}
				});

				if (!fileConfig.header) throw `no config for file ${fullPath}`;

				const workbook = XLSX.readFile(fullPath);
				const sheet = workbook.Sheets[workbook.SheetNames[0]];
				const xlsData = XLSX.utils.sheet_to_json(sheet, { range: 1, header: fileConfig.header });

				xlsData.forEach(row => {
					const a = mapData[row.id];

					if (!row['bank'] && fileConfig.defaults) row['bank'] = fileConfig.defaults.bank;

					row['file'] = file;
					if (a) {
						row['latitude'] = a.latitude;
						row['longitude'] = a.longitude;
					} else {
						dataNotFound++;
					}
					if (row.id) {
						data.push(row);
					} else {
						console.error(`no id ${JSON.stringify(row)}`);
					}
				});
			}
			catch (err) {
				console.error(err);
			}
		});

		console.log(`fileData.length=${data.length}`);
		console.log(`dataNotFound=${dataNotFound}`);

		const saveFileName = path.join(dir, 'test.xlsx');

		const wb = { SheetNames: [], Sheets: {} };
		const ws = XLSX.utils.json_to_sheet(data);
		XLSX.utils.book_append_sheet(wb, ws, 'atm_data');
		XLSX.writeFile(wb, saveFileName);
		console.log(`saveFileName=${saveFileName}`);
	})
	.catch(err => {
		console.error(err);
	});
