const uuid4 = require('uuid').v4;
const mysql = require('mysql');
const config = require('/app/ab-app-config.json');
const db = mysql.createConnection(config.mysql);
const fs = require('fs');
const csv = require('csv');
const iconv = require('iconv-lite');

db.connect();

db.query('truncate table MAP_ATM');

fs.createReadStream('./atm.insert.csv')
	.pipe(iconv.decodeStream('win1251'))
	.pipe(csv.parse({ delimiter: ';' }, (err, data) => {
		if (err) throw err;

		let totalInserted = 0;
		let totalError = 0;

		for (const a in data) {
			const d = data[a];

			let item = {};
			item._id = uuid4();
			item._created = new Date().toLocaleString();
			item._updated = new Date().toLocaleString();

			item.uid = d[0];
			item.description = d[1];
			item.region = d[2];
			item.city = d[3];
			item.address = d[4];
			item.razmeshenie = d[5];
			item.schedule = d[6];
			item.type = d[7];
			item.longitude = parseFloat(d[8].replace(/,/g, '.'));
			item.latitude = parseFloat(d[9].replace(/,/g, '.'));
			item.division = 3;

			db.query('INSERT INTO MAP_ATM (id,json_data) VALUES (?,?)', [item.uid, JSON.stringify(item)], function (err) {
				if (err) console.error(item, err);
			});
		}
		db.end();
	}));
/*
for (const e in all) {
    const i = all[e];
    i._id = uuid4();
    i._created = new Date().toLocaleString();
	i._updated = new Date().toLocaleString();

    db.query("INSERT INTO MAP VALUES (?)", JSON.stringify(i), function (err) {
        if (err) throw err;
    });
    console.log(i);
}
*/

//db.end();