const all = require('./all.json');
const uuid4 = require('uuid').v4;
const mysql = require('mysql');
const config = require('/app/ab-app-config.json');
const db = mysql.createConnection(config.mysql);

db.connect();

db.query('truncate table MAP');

for (const e in all) {
	const i = all[e];
    
	if (!i._id) i._id = uuid4();
	if (!i._created) i._created = new Date().toLocaleString();
	if (!i._updated) i._updated = new Date().toLocaleString();

	db.query('INSERT INTO MAP VALUES (?)', JSON.stringify(i), function (err) {
		if (err) throw err;
	});
	console.log(i);
}

db.end();

console.log('inserted');