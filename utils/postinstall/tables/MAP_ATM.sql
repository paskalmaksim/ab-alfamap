CREATE TABLE IF NOT EXISTS `MAP_ATM` (
  `id` varchar(45) CHARACTER SET utf8 NOT NULL,
  `json_data` json NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
)