CREATE TABLE IF NOT EXISTS `MAP_SEARCH_LOG` (
  `search_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `search` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `result` json NOT NULL,
  `cache` varchar(45) CHARACTER SET utf8 DEFAULT 'false'
)