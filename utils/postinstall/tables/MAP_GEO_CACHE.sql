CREATE TABLE IF NOT EXISTS  `MAP_GEO_CACHE` (
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cache` json NOT NULL,
  `cache_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`address`),
  UNIQUE KEY `idMAP_GEO_cache_UNIQUE` (`address`)
)