const uuid4 = require('uuid').v4;
const mysql = require('mysql');
const path = require('path');
const config = require('/app/ab-app-config.json');
const db = mysql.createConnection(config.mysql);
const logger = require('pino')({ name: 'insert' });
const XLSX = require('xlsx');

const MapPointTobeHeader = [
	'uid',
	'bank',
	'region',
	'city',
	'address',
	'status',
	'statusText',
	'scheduleType',
	'schedule',
	'type',
	'latitude',
	'longitude'
];

const workbook = XLSX.readFile(path.join(__dirname, 'MapPointResults.xlsx'));
const sheet = workbook.Sheets[workbook.SheetNames[0]];
const xlsData = XLSX.utils.sheet_to_json(sheet, { range: 1, header: MapPointTobeHeader });

logger.info(`xlsData.length=${xlsData.length}`);

db.connect();

db.query('truncate table MAP_ATM', err => {
	if (err) {
		logger.error(err);
		process.exit(-1);
	}
	logger.info('truncate done');
});

const xlsDataLength = xlsData.length;
let xlsDataInsertedLength = 0;
let errorsCount = 0;

xlsData.forEach(row => {
	let item = {};
	item._id = uuid4();
	item._created = new Date().getTime();
	item._updated = item._created;

	item.division = 3;

	item.uid = row.uid;
	item.description = row.bank;
	item.city = row.city;
	item.address = row.address;

	if (row.region) item.region = row.region;
	if (row.status) item.status = row.status;
	if (row.statusText) item.statusText = row.statusText;
	if (row.scheduleType) item.scheduleType = row.scheduleType;
	if (row.schedule) item.schedule = row.schedule;
	if (row.type) item.type = row.type;

	item.longitude = row.longitude;
	item.latitude = row.latitude;

	db.query('INSERT INTO MAP_ATM (id,json_data) VALUES (?,?)', [item.uid, JSON.stringify(item)], err => {
		if (err) {
			//logger.error(item, err);
			errorsCount++;
		}
		xlsDataInsertedLength++;
	});
});

const timer = setInterval(() => {
	if (xlsDataInsertedLength == xlsDataLength) {
		clearInterval(timer);
	}
	logger.info(`${xlsDataInsertedLength}/${xlsDataLength}/errorsCount=${errorsCount}`);
}, 1000);

db.end();