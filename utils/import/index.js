
const XLSX = require('xlsx');
const fs = require('fs');
const path = require('path');
const config = require('/app/ab-app-config.json');
const mysql = require('mysql');
const logger = require('pino')({ name: 'import' });
const request = require('request').defaults({
	strictSSL: false,
	proxy: 'http://127.0.0.1:3139',
	pool: {
		maxSockets: 2
	}
});

const getMapData = () => {
	return new Promise((resolve, reject) => {
		let mapData = [];
		const db = mysql.createConnection(config.mysql);
		db.query('SELECT json_data FROM MAP_ATM', function (err, result) {
			if (err) reject(err);
			for (const e in result) {
				const a = JSON.parse(result[e].json_data);
				mapData[a.uid + a.description] = a;
			}
			resolve(mapData);
		});
		db.end();
	});
};
const startAsync = async () => {
	try {
		const mapData = await getMapData();
		logger.info(`mapData.length=${Object.keys(mapData).length}`);
		let data = [];
		let dataNotFound = 0;

		const MapPointTobeHeader = [
			'uid',
			'bank',
			'region',
			'city',
			'address',
			'status',
			'statusText',
			'scheduleType',
			'schedule',
			'type'
		];

		let MapPointTobeHeaderResults = MapPointTobeHeader;
		MapPointTobeHeaderResults.push('latitude');
		MapPointTobeHeaderResults.push('longitude');

		MapPointTobeHeaderResults.push('geo_latitude');
		MapPointTobeHeaderResults.push('geo_longitude');
		MapPointTobeHeaderResults.push('geo_formatted_address');
		MapPointTobeHeaderResults.push('geo_city');
		MapPointTobeHeaderResults.push('geo_region');
		MapPointTobeHeaderResults.push('geo_addr');

		/* backup */
		const backupMapPoint = () => {
			let backupData = [];

			for (const i in mapData) {
				const item = mapData[i];

				backupData.push({
					'uid': item.uid,
					'bank': item.description,
					'region': item.region,
					'city': item.city,
					'address': item.address,
					'status': item.status,
					'statusText': item.statusText,
					'scheduleType': item.scheduleType,
					'schedule': item.schedule,
					'type': item.type,
					'latitude': item.latitude,
					'longitude': item.longitude
				});
			}

			const backupFileName = path.join(__dirname, 'backup', `MapPointBackup${new Date().getTime()}.xlsx`);
			const wb = { SheetNames: [], Sheets: {} };
			const ws = XLSX.utils.json_to_sheet(backupData);
			XLSX.utils.book_append_sheet(wb, ws, 'backup');
			XLSX.writeFile(wb, backupFileName);
			logger.info(`backupFileName=${backupFileName}`);
		};

		backupMapPoint();

		function getXLSData(fileName) {
			const workbook = XLSX.readFile(path.join(__dirname, fileName));
			const sheet = workbook.Sheets[workbook.SheetNames[0]];
			return XLSX.utils.sheet_to_json(sheet, { range: 1, header: MapPointTobeHeader });
		}

		const addressNotFound = [];
		let xlsData = [];

		xlsData = xlsData.concat(getXLSData('MapPointLocal.xlsx'));
		xlsData = xlsData.concat(getXLSData('MapPointForeign.xlsx'));

		logger.info(`xlsData.length=${xlsData.length}`);

		let allUID = [];

		let UIDWarnings = 0;
		xlsData.forEach(row => {
			try {
				if (!row.uid) throw 'no uid';
				if (!row.bank) throw 'no bank';

				row.uid = row.uid.trim();
				row.bank = row.bank.trim();

				const UID = `${row.uid}${row.bank}`;

				if (allUID.includes(UID)) {
					UIDWarnings++;
				} else {
					allUID.push(UID);

					const map = mapData[UID];

					if (map) {
						const newAddress = `${row.city}, ${row.address}`;
						const oldAddress = `${map.city}, ${map.address}`;

						if (!map.latitude || !map.longitude) {
							addressNotFound.push(row);
						} else if (newAddress == oldAddress) {
							row['latitude'] = map.latitude;
							row['longitude'] = map.longitude;

							data.push(row);
						} else {
							addressNotFound.push(row);
						}
					} else {
						addressNotFound.push(row);
					}
				}
			} catch (err) {
				logger.error(row, err);
			}
		});

		let addressFoundLength = 0;

		const findAddress = (row) => {
			return new Promise((resolve, reject) => {
				let address = row.address;

				if (row.address.startsWith(row.city)) {
					address = row.address.substring(row.city.length + 1).trim();
				}

				const options = {
					url: 'https://maps.googleapis.com/maps/api/geocode/json',
					qs: {
						key: 'AIzaSyD8ComL6Ubz-l_6KIbjVjcbSzHL9NyQbHI',
						address: `Украина, ${row.city}, ${address}`.trim(),
						'components': 'country:UA',
						'language': 'ru',
						'region': 'ua'
					}
				};

				request(options, (error, response, body) => {
					if (error) {
						logger.error(row, error);
					} else {
						try {
							const result = JSON.parse(body);

							const resultCount = result.results.length;
							if (resultCount == 1) {
								addressFoundLength++;

								const location = result.results[0].geometry.location;

								row['geo_latitude'] = location.lat.toString().replace(/,/g, '.');
								row['geo_longitude'] = location.lng.toString().replace(/,/g, '.');
								row['geo_formatted_address'] = result.results[0].formatted_address;

								let address = {};
								result.results[0].address_components.forEach(a => {
									address[a['types'][0]] = a['long_name'];
								});

								let city = address.locality;
								let region = address.administrative_area_level_1;
								if (!region) region = address.administrative_area_level_2;
								let addr = address.route;
								if (address.street_number) addr += `, ${address.street_number}`;

								row['geo_city'] = city;
								row['geo_region'] = region;
								row['geo_addr'] = addr;
							}
						} catch (e) {
							logger.error(row, e);
						}
					}
					data.push(row);
					resolve();
				});
			});
		};
		logger.info(`UIDWarnings=${UIDWarnings}`);
		logger.info(`addressNotFound.length=${addressNotFound.length}`);

		/* using sync method for performance issue, www proxy fail when "await Promise.all(addressNotFound.map(findAddress))"*/

		for (const i in addressNotFound) {
			//logger.info(`${i}/${addressNotFound.length}/${addressNotFound[i].uid}/Украина, ${addressNotFound[i].city}, ${addressNotFound[i].address}`);
			//await findAddress(addressNotFound[i]);
		}

		logger.info(`addressFoundLength=${addressFoundLength}`);

		const saveFileName = path.join(__dirname, 'MapPointResults.xlsx');
		const wb = { SheetNames: [], Sheets: {} };
		const ws = XLSX.utils.json_to_sheet(data, { header: MapPointTobeHeaderResults });
		XLSX.utils.book_append_sheet(wb, ws, 'atm_data');
		XLSX.writeFile(wb, saveFileName);

		logger.info(`saveFileName=${saveFileName}`);
	} catch (err) {
		logger.error(err);
	}
};

startAsync();