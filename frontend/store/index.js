import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = () => new Vuex.Store({
    state: {
        token: 'qwerty',
        version: process.env.version
    },
    mutations: {
        SET_TOKEN(state, token) {
            state.token = token;
        },
        async login(state, auth) {
            //const { data } = await axios.post('/api/login.json');
            state.token = 'qwerty';
        },
        logout(state) {
            state.token = null;
        }
    },
    actions: {
        nuxtServerInit(commit, req) {
            if (req.session && req.session.token) commit('SET_TOKEN', req.session.token);
        }
    }
})

export default store