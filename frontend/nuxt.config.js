
const { version } = require('./package.json');
const basePath = '/map';

let nuxtConfig = {
  mode: 'spa',
  css: [
    'bootstrap/dist/css/bootstrap.min.css'
  ],
  head: {
    title: 'Карта назначений',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Поиск объектов на карте' }
    ]
  },
  env: {
    googlekey: 'AIzaSyDYGCa10yERPfdtLhngizD4SNZGKgUjRyA'
  },
  plugins: [
    '~/plugins/bootstrap-vue'
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  router: {
    base: `${basePath}`
  },
  axios: {
    baseURL: ''
  },
  generate: {
    dir: `dist/${basePath}`,
    minify: {
      removeComments: true
    }
  },
  build: {
    publicPath: `/${version}/`
  },
  loading: {
    color: '#77b6ff',
    height: '3px'
  },
  loadingIndicator: '~/components/loading.html'
}

if (process.env.NODE_ENV && process.env.NODE_ENV.trim() === 'dev') {
  nuxtConfig.modules.push('@nuxtjs/proxy');
  nuxtConfig.proxy = {};
  nuxtConfig.proxy[`${basePath}/api`] = 'http://localhost:8090';
}

module.exports = nuxtConfig;