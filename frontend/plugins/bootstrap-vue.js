import Vue from 'vue'

import bNavbar from 'bootstrap-vue/es/components/navbar/navbar';
import bNavbarBrand from 'bootstrap-vue/es/components/navbar/navbar-brand';
//import bTooltip from 'bootstrap-vue/es/components/tooltip/tooltip';
import bCard from 'bootstrap-vue/es/components/card/card';
import bForm from 'bootstrap-vue/es/components/form/form';
import bInputGroup from 'bootstrap-vue/es/components/input-group/input-group';
import bFormInput from 'bootstrap-vue/es/components/form-input/form-input';
import bButton from 'bootstrap-vue/es/components/button/button';
import bProgress from 'bootstrap-vue/es/components/progress/progress';
import bAlert from 'bootstrap-vue/es/components/alert/alert';
import bListGroup from 'bootstrap-vue/es/components/list-group/list-group';
import bListGroupItem from 'bootstrap-vue/es/components/list-group/list-group-item';
import bFormCheckboxGroup from 'bootstrap-vue/es/components/form-checkbox/form-checkbox-group';
import bTabs from 'bootstrap-vue/es/components/tabs/tabs';
import bTab from 'bootstrap-vue/es/components/tabs/tab';

Vue.component('b-navbar', bNavbar);
Vue.component('b-navbar-brand', bNavbarBrand);
//Vue.component('b-tooltip', bTooltip);
Vue.component('b-card', bCard);
Vue.component('b-form', bForm);
Vue.component('b-input-group', bInputGroup);
Vue.component('b-form-input', bFormInput);
Vue.component('b-button', bButton);
Vue.component('b-progress', bProgress);
Vue.component('b-alert', bAlert);
Vue.component('b-list-group', bListGroup);
Vue.component('b-list-group-item', bListGroupItem);
Vue.component('b-form-checkbox-group', bFormCheckboxGroup);
Vue.component('b-tabs', bTabs);
Vue.component('b-tab', bTab);